function calculate() {
    let miles  = document.getElementById('miles').value;
    if (miles == "") {
        alert ("You must write miles");
        return;
    } 
    
    else 
    
    {
    let kilometers = miles * 1.609344;
        meters = kilometers * 1000;
        centimeters = meters * 100;
        
    document.getElementById('kilometers').innerHTML = "Distance in kilometers: " + kilometers.toFixed(1);
    document.getElementById('meters').innerHTML = "Distance in meters: " + meters.toFixed(1);
    document.getElementById('centimeters').innerHTML = "Distance in centimeters: " + centimeters.toFixed(1);
    }
}