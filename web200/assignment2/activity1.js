// This program counts the weekly, monthly, and annual salary based on the hours and rate per hour.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

function calculate() {
    let hours  = document.getElementById('hours').value;
    let rate = document.getElementById('rate').value;

    if (hours == "") {
        alert ("You must write hours");
        return;
    } 
    
    if (rate == "") {
        alert ("You must write rate");
        return;
    }
    
    let pay = hours * rate;
    let weekly = rate * 40;
    let monthly = weekly * 4;
    let annual = weekly * 52;

    document.getElementById('payForThisHours').innerHTML = 
        "Pay for hours, that you write: " + pay;
    document.getElementById('weekPay').innerHTML = 
        "Weekly pay: " + weekly;
    document.getElementById('monthPay').innerHTML = 
        "Monthly pay: " + monthly;
    document.getElementById('annualPay').innerHTML = 
        "Annual pay: " + annual;
}
