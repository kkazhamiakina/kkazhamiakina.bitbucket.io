// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

function calculate() {
    let years  = document.getElementById('years').value;
    if (years == "") 
    {
    alert ("You must write years");
    return;
    
    } 
    
    else 
    
    {
    
    let month = years * 12;
        days = years * 365;
        hours = days * 24;
        seconds = hours * 3600;
        
    document.getElementById('personMonths').innerHTML = "Person is " + month + " months old";
    document.getElementById('personDays').innerHTML = "Person is " + days + " days old";
    document.getElementById('personHours').innerHTML = "Person is " + hours + " hours old";
    document.getElementById('personSeconds').innerHTML = "Person is " + seconds + " seconds old";
    }
}