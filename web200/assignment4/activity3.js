// This program converts miles into the kilometers, meters, and centimeters.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

window.addEventListener("load", function () {
    document.getElementById("miles").addEventListener("input", main);
});

function main() {
    let miles = getMiles();

    let kilometers = milesToKilometers(miles);

    let meters = milesToMeters(kilometers);

    let centimeters = milesToCentimeters(meters);

    displayResults(kilometers, meters, centimeters);
}

function getMiles() {
    let miles = document.getElementById("miles").value;
    miles = Number(miles);
    return miles;
}

function milesToKilometers(miles) {
    let kilometers = miles * 1.609344;
    return kilometers;
}

function milesToMeters(kilometers) {
    let meters = kilometers * 1000;
    return meters;
}
function milesToCentimeters(meters) {
    let centimeters = meters * 100;
    return centimeters;
}

function displayResults(kilometers, meters, centimeters) {
    document.getElementById("kilometers").innerText = kilometers.toFixed(1);
    document.getElementById("meters").innerText = meters.toFixed(2);
    document.getElementById("centimeters").innerText = Math.round(centimeters);
}
