// This program counts the weekly, monthly, and annual salary based on the hours and rate per hour.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

window.addEventListener("load", function () {
    document.getElementById("rate").addEventListener("input", main);
});

function main() {
    let hours = getHours();
    let rate = getRate();

    let pay = perHoursYouWrite(hours, rate);
    let weekly = weeklyPay(rate);
    let monthly = monthlyPay(weekly);
    let annual = annualPay(weekly);

    displayResults(pay, weekly, monthly, annual);
}

function getHours() {
    let hours = document.getElementById("hours").value;
    hours = Number(hours);
    return hours;
}

function getRate() {
    let rate = document.getElementById("rate").value;
    rate = Number(rate);
    return rate;
}

function perHoursYouWrite(hours, rate) {
    let pay = hours * rate;
    return pay;
}

function weeklyPay(rate) {
    let weekly = rate * 40;
    return weekly;
}

function monthlyPay(weekly) {
    let monthly = weekly * 4;
    return monthly;
}

function annualPay(weekly) {
    let annual = weekly * 52;
    return annual;
}

function displayResults(pay, weekly, monthly, annual) {
    document.getElementById("pay").innerText = pay;
    document.getElementById("weekly").innerText = weekly;
    document.getElementById("monthly").innerText = monthly;
    document.getElementById("annual").innerText = annual;
}
