// This program displays the results from the array using dynamic HTML to create and append HTML elements after processing is complete.

// References:
//https://en.wikibooks.org/wiki/JavaScript
//https://en.wikibooks.org/wiki/JavaScript/DHTML
//https://en.wikipedia.org/wiki/Dynamic_HTML
//https://www.youtube.com/watch?v=XFGtGzZhy_A
//https://www.youtube.com/watch?v=FbcB8CroByk
//https://www.youtube.com/watch?v=HvWnkj8nh8Y
//https://www.youtube.com/watch?v=_OOj_d3Zdig
//https://www.w3schools.com/js/js_htmldom_nodelist.asp
//https://www.w3schools.com/js/js_htmldom_collections.asp
//https://www.w3schools.com/js/js_htmldom_nodes.asp
//https://www.w3schools.com/js/js_htmldom_navigation.asp
//https://www.w3schools.com/js/js_htmldom_animate.asp
//https://www.w3schools.com/js/js_htmldom_css.asp
//https://www.w3schools.com/js/js_htmldom_html.asp

"use strict";
            
window.addEventListener("load", function () {
    document.getElementById("startingValue").addEventListener("input", checkScores);
    document.getElementById("endingValue").addEventListener("input", checkScores);
});
function checkScores() {
    let id = document.activeElement.id;
    let value = document.getElementById(id).value;
    if (isNaN(value) || value.trim().length == 0) {
        let error = document.getElementById("error");
        let errorElem = document.createElement("p");
        let errorMessage = "Write a number!";

        errorElem.innerHTML = errorMessage;
        error.appendChild(errorElem);

        return false;
    } else if (value > 10) {
        let error = document.getElementById("error");
        let errorElem = document.createElement("p");
        let errorMessage = "You can calculate maximum 10!";

        errorElem.innerHTML = errorMessage;
        error.appendChild(errorElem);

        return false;
    } else {
        document.getElementById("error").innerText = "";
        return true;
    }
}

function createTable(){
    let rows = document.getElementById("startingValue").value;
    let cols = document.getElementById("endingValue").value;
    let j = rows;
    let output = "<table border='1' width='500' cellspacing='0'cellpadding='5'>";
    let x = 0;
    let arr = [];
    let arr1 = [];
    
    for (let i = rows - 1; i <= cols; i++) {
        if (x == 0) {
            arr1.push("");
            x = x + 1; 
        } else {
            arr1.push(i);
            x = x + 1; 
        };
    };
    arr.push(arr1)
    for (let i = rows; i <= cols; i++) {
        let arr2 = [];
        arr2.push(i);
        for (j = rows; j <= cols; j++) {
            arr2.push(i * j);
        };
        arr.push(arr2)
    };

    for (let i = 0; i <= arr.length-1; i++) {
        output = output + "<tr>";
        for (let j = 0; j <= arr[i].length-1; j++) {
            output = output + "<td>" + arr[i][j] + "</td>";
        };
    };
    const table = document.createElement("P");
    table.innerHTML = output;
    document.body.appendChild(table);
};