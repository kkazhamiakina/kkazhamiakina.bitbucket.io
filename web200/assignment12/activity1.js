// This program displays the results from the array using dynamic HTML to create and append HTML elements after processing is complete.

// References:
//https://en.wikibooks.org/wiki/JavaScript
//https://en.wikibooks.org/wiki/JavaScript/DHTML
//https://en.wikipedia.org/wiki/Dynamic_HTML
//https://www.youtube.com/watch?v=XFGtGzZhy_A
//https://www.youtube.com/watch?v=FbcB8CroByk
//https://www.youtube.com/watch?v=HvWnkj8nh8Y
//https://www.youtube.com/watch?v=_OOj_d3Zdig
//https://www.w3schools.com/js/js_htmldom_nodelist.asp
//https://www.w3schools.com/js/js_htmldom_collections.asp
//https://www.w3schools.com/js/js_htmldom_nodes.asp
//https://www.w3schools.com/js/js_htmldom_navigation.asp
//https://www.w3schools.com/js/js_htmldom_animate.asp
//https://www.w3schools.com/js/js_htmldom_css.asp
//https://www.w3schools.com/js/js_htmldom_html.asp

"use strict";
window.addEventListener("load", function () {
    document.getElementById("howMuchScores").addEventListener("input", checkScores);
});

function checkScores() {
    let scoreValue = document.getElementById("howMuchScores").value;
    if (isNaN(scoreValue) || scoreValue.trim().length == 0) {
        clearInputs();
        let error = document.getElementById("error");
        let errorElem = document.createElement("p");
        let errorMessage = "Write a number!";

        errorElem.innerHTML = errorMessage;
        error.appendChild(errorElem);
        
        return false;
    } else if (scoreValue > 5) {
        clearInputs();
        let error = document.getElementById("error");
        let errorElem = document.createElement("p");
        let errorMessage = "You can calculate maximum 5 scores!";

        errorElem.innerHTML = errorMessage;
        error.appendChild(errorElem);
        
        return false;
    } else {
        document.getElementById("error").innerText = "";
        clearInputs();
        return true;
    }
}

function enterScore() {
    let howMuchScores = document.getElementById("howMuchScores").value;
    let i;
    let arr = [];
    let score = 0;
    let sum = 0;

    for ( i = 0; i < +howMuchScores; i++) {
        score = prompt('Write down your scores one by one');
        arr.push(+score);
    }

    arr.forEach(function(item) {
        sum = sum + item;
    });

    let result = document.getElementById("result");

    let finalResult = document.createElement("p");

    let finalValue = Math.round( sum / arr.length );

    finalResult.innerHTML = finalValue;

    result.appendChild(finalResult);
}

function clearInputs() {
    document.getElementById("result").innerText = "";
}