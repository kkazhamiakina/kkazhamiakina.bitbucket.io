// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

window.addEventListener("load", function () {
    document.getElementById("miles").addEventListener("input", transformMiles);
});

function transformMiles() {
    let id = document.activeElement.id;
    
    if (id == 'kilometers') {
        milesToKilometers();
    } else if (id == 'meters') {
        milesToMeters(); 
    } else if (id == 'centimeters') {
        milesToCentimeters();
    } 
}

function checkInput(value) {
    if (isNaN(value) || value.trim().length == 0) {
        document.getElementById("error").innerText = "Write a number!";
        return false;
    } else {
        document.getElementById("error").innerText = "";
        return true;
    }
}

function getMiles() {
    let miles = document.getElementById("miles").value;
    checkInput(miles);
    return miles;
}

function milesToKilometers() {
    let kilometers = getMiles() * 1.609344;
    displayResult(kilometers);
    return kilometers;
}
function milesToMeters() {
    let meters = getMiles() * 1.609344 * 1000;
    displayResult(meters);
    return meters;
}
function milesToCentimeters() {
    let centimeters = milesToMeters() * 100;
    displayResult(centimeters);
}

function displayResult(result) {
    document.getElementById("result").innerText = result;
}