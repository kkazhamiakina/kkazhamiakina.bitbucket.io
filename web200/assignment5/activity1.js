// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

window.addEventListener("load", function () {
    document.getElementById("years").addEventListener("input", checkTime);
});

function checkTime() {
    let id = document.activeElement.id;
    
    switch (id) {
        case 'month':
            calculateMonth();
            break;
        case 'days':
            calculateDays();
            break;
        case 'hours':
            calculateHours();
            break;
        case 'minutes':
            calculateMinutes();
            break;
        case 'seconds':
            calculateSeconds();
            break;
        default:
        console.log("Unexpected active element: " + id);
    }
}

function checkInput(value) {
    if (isNaN(value) || value.trim().length == 0) {
        document.getElementById("error").innerText = "Write a number!";
        return false;
    } else {
        document.getElementById("error").innerText = "";
        return true;
    }
}

function getYears() {
    let years = document.getElementById("years").value;
    checkInput(years);
    return years;
}

function calculateMonth() {
    let month = getYears() * 12;
    displayResult(month);
}
function calculateDays() {
    let days = getYears() * 365;
    displayResult(days);
}
function calculateHours() {
    let hours = getYears() * 365 * 24;
    displayResult(hours);
}
function calculateMinutes(hours) {
    let minutes = getYears() * 365 * 24 * 60;
    displayResult(minutes);
}
function calculateSeconds(minutes) {
    let seconds = getYears() * 365 * 24 * 60 * 60;
    displayResult(seconds);
}

function displayResult(result) {
    document.getElementById("result").innerText = result;
}