// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

main();

function main() {
    let years = getYears();

    let month = yearsToMonth(years);
    let days = yearsToDays(years);
    let hours = yearsToHours(days);
    let minutes = yearsToMinutes(hours);
    let seconds = yearsToSeconds(minutes);

    displayResults(month, days, hours, minutes, seconds);
}

function getYears() {
    let years = window.prompt("Enter years: ");
    years = Number(years);
    return years;
}

function yearsToMonth(years) {
    let month = years * 12;
    return month;
}
function yearsToDays(years) {
    let days = years * 365;
    return days;
}
function yearsToHours(days) {
    let hours = days * 24;
    return hours;
}
function yearsToMinutes(hours) {
    let minutes = hours * 60;
    return minutes;
}
function yearsToSeconds(minutes) {
    let seconds = minutes * 60;
    return seconds;
}

function displayResults(month, days, hours, minutes, seconds) {
    document.getElementById("month").innerText = month;
    document.getElementById("days").innerText = days;
    document.getElementById("hours").innerText = hours;
    document.getElementById("minutes").innerText = minutes;
    document.getElementById("seconds").innerText = seconds;
}