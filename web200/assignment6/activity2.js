// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

function enterScore() {
    let sumScore = 0;
    let score = 0;
    let boolFlag = true;
    let counter = 0;

    do {
        score = prompt('Write down your scores one by one. To stop entering scores just send an empty field!');
        if (score.trim().length == 0) {
            boolFlag = false;
        } else {
            sumScore = +sumScore + +score;
            counter = counter + 1;
        }
    }
    while ( boolFlag == true );
    document.getElementById("result").innerText = Math.round(sumScore/counter);
}

function clearInputs() {
    document.getElementById("result").innerText = "";
}