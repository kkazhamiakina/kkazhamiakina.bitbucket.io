// This program demonstrates JavaScript form validation.
//
// References:
//   https://en.wikibooks.org/wiki/JavaScript
//   https://www.w3schools.com/js/js_validation.asp
//   https://www.w3schools.com/tags/att_form_enctype.asp
//   https://www.w3schools.com/js/js_validation_api.asp
//   https://www.youtube.com/watch?v=Bl59zGI1gCA
//   https://www.youtube.com/watch?v=AZj2unIbWCM
//   https://www.youtube.com/watch?v=HpMnYWQSxJE
//   https://www.youtube.com/watch?v=gM8I7NywAcg
//   https://www.youtube.com/watch?v=bSQ1iwioapc
//   https://www.youtube.com/watch?v=Xy1K8ODZC8w
//   https://www.youtube.com/watch?v=In0nB0ABaUk

"use strict";
window.addEventListener("load", function () {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("focus", inputFocus);
    elements[i].addEventListener("input", inputInput);
  }
  
  document.getElementById("get").addEventListener("click", getClick);
  document.getElementById("post").addEventListener("click", postClick);

  document.getElementById("fname").focus();
});

function inputFocus() {
  document.activeElement.select();
  displayPrompt();
  checkButtons();
}

function displayPrompt(id = null) {
  const prompts = {
    fname: "Enter your first name",
    lname: "Enter your last name",
    address: "Enter your address",
    city: "Enter your city",
    region: "Enter your region",
    postalCode: "Enter your postal code as xxxxx",
    phoneNumber: "Enter your phone number as xxx-xx-xxxx",
    dateOfBirth: "Enter your date of birth as xx-xx-xxxx"
  }

  let elements = document.getElementsByTagName("output");

  if (id == null) {
    id = document.activeElement.id;

    for (let i = 0; i < elements.length; i++) {
      elements[i].innerText = "";
    }
  }

  try {
    document.getElementById(id + "-prompt").innerText = prompts[id];
  }
  catch {
    // ignore when the active element is a button
  }
}

function inputInput() {
  let element = document.activeElement;
  let id = element.id;

  if (!element.checkValidity()) {
    document.getElementById(id + "-prompt").innerText = element.validationMessage;
  }
  else {
    document.getElementById(id + "-prompt").innerText = "";
  }
  checkButtons();
}


function checkButtons() {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    if (!elements[i].checkValidity()) {
      document.getElementById("get").disabled = true;
      document.getElementById("post").disabled = true;
      return;
    }
  }

  document.getElementById("get").disabled = false;
  document.getElementById("post").disabled = false;
}

function getClick() {
  let form = document.getElementById("form")
  form.action = "http://postman-echo.com/get";
  form.method = "GET";
  form.submit();
}

function postClick() {
  let form = document.getElementById("form")
  form.action = "http://postman-echo.com/post";
  form.method = "POST";
  form.submit();
}