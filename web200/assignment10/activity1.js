// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";
function BookObject(title = null, author = null, year = null, publisher = null, city = null, state = null ) {
    this.title = title;
    this.author = author;
    this.year = year;
    this.publisher = publisher;
    this.city = city;
    this.state = state;
}

window.addEventListener("load", function () {
    let elements = document.getElementsByTagName("input");
  
    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("focus", inputFocus);
        elements[i].addEventListener("input", inputInput);
    }
  
    document.getElementById("title").focus();
});
  
function inputFocus() {
    document.activeElement.select();
  
    document.getElementById("error").innerText = 
        "Enter " + document.activeElement.id + " value.";
}
  
function inputInput() {
    let value = document.activeElement.value;
  
    if (checkInput()) {
        document.getElementById("error").innerText = "";
        let book = createBook();

        displayBook(book);
    }
}
  
function checkInput() {
    let value = document.activeElement.value;

    if (value.trim().length == 0) {
      document.getElementById("error").innerText = 
        document.activeElement.id + " must be a number!";
      return false;
    }
  
    let elements = document.getElementsByTagName("input");
    for (let i = 0; i < elements.length; i++) {
      value = elements[i].value;
      if (value.trim().length == 0) {
        return false;
      }
    }
    
    return true;
}

function createBook() {
    let titleValue = document.getElementById("title").value;
    let authorValue = document.getElementById("author").value;
    let yearValue = document.getElementById("year").value;
    let publisherValue = document.getElementById("publisher").value;
    let cityValue = document.getElementById("city").value;
    let stateValue = document.getElementById("state").value;

    let book = [];

    let createBook = new BookObject();

    createBook.title = titleValue;
    createBook.author = authorValue;
    createBook.year = yearValue;
    createBook.publisher = publisherValue;
    createBook.city = cityValue;
    createBook.state = stateValue;

    let objToArr = Object.values(createBook);

    book.push(objToArr);

    return book;
}

function displayBook(book) {
    let result = "";
    console.log(book);
    for (let i = 0; i < book.length; i++) {
      result += `<p> ${book[i]} </p> `;
    }
    document.getElementById("result").innerHTML = result;
}