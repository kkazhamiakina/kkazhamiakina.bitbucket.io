// This program converts years into the months, days, hours, and seconds.
//
// References:
//   https://www.mathsisfun.com/temperature-conversion.html
//   https://en.wikibooks.org/wiki/JavaScript

// Use your browser's built-in web development tools to view console output.

"use strict";

let books = [];

window.addEventListener("load", function () {
    document.getElementById("apaBtn").addEventListener("click", createBook);
});


function BookObject(title = null, author = null, year = null, 
    publisher = null, state = null, city = null) {

    this.title = title;
    this.author = author;
    this.year = year;
    this.publisher = publisher;
    this.state = state;
    this.city = city;

    this.displayApa = function() {
        return `${this.author}. (${this.year}). 
        ${this.title}. ${this.state}, ${this.city}: ${this.publisher}`;
    }
}

window.addEventListener("load", function () {
    let elements = document.getElementsByTagName("input");
  
    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener("focus", inputFocus);
        elements[i].addEventListener("input", inputInput);
    }
  
    document.getElementById("title").focus();
});
  
function inputFocus() {
    document.activeElement.select();
  
    document.getElementById("error").innerText = 
        "Enter " + document.activeElement.id + " value.";
}
  
function inputInput() {
    let value = document.activeElement.value;
  
    if (checkInput()) {
        document.getElementById("error").innerText = "";
        let book = createBook();

        displayBooks(book);
    }
}
  
function checkInput() {
    let value = document.activeElement.value;

    if (value.trim().length == 0) {
      document.getElementById("error").innerText = 
        document.activeElement.id + " must be a number!";
      return false;
    }
  
    let elements = document.getElementsByTagName("input");
    for (let i = 0; i < elements.length; i++) {
      value = elements[i].value;
      if (value.trim().length == 0) {
        return false;
      }
    }
    
    return true;
}

function createBook() {
    let titleValue = document.getElementById("title").value;
    let authorValue = document.getElementById("author").value;
    let yearValue = document.getElementById("year").value;
    let publisherValue = document.getElementById("publisher").value;
    let cityValue = document.getElementById("city").value;
    let stateValue = document.getElementById("state").value;
    
    let book = new BookObject();

    book.title = titleValue;
    book.author = authorValue;
    book.year = yearValue;
    book.publisher = publisherValue;
    book.state = stateValue;
    book.city = cityValue;
    
    books.push(book);
}

function displayBooks() {
    let result = "";

    for (let i = 0; i < books.length; i++) {
      result += `<p> ${books[i].displayApa()} </p> `;
    }
    
    document.getElementById("result").innerHTML = result;
}
