// This program demonstrates JavaScript AJAX and JSON.
//
// References:
//   https://www.w3schools.com/js/js_ajax_intro.asp
//   https://www.w3schools.com/js/js_json_intro.asp
//   https://www.youtube.com/watch?v=wI1CWzNtE-M
//   https://www.youtube.com/watch?v=rJesac0_Ftw
//   https://en.wikibooks.org/wiki/JavaScript/XMLHttpRequest
//   https://en.wikibooks.org/wiki/JavaScript/Handling_JSON
//   https://en.wikipedia.org/wiki/JSON
//   https://en.wikipedia.org/wiki/Ajax_(programming)

"use strict";

function User(fname=null, lname=null, address=null, city=null, region='Chicago', postalCode=null, phoneNumber=null, dateOfBirth=null) {
  this.firstName = fname;
  this.lastName = lname;
  this.address = address;
  this.city = city;
  this.region = region;
  this.postalCode = postalCode;
  this.phoneNumber = phoneNumber;
  this.dateOfBirth = dateOfBirth;
}

window.addEventListener("load", function () {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("focus", inputFocus);
    elements[i].addEventListener("input", inputInput);
  }
  
  document.getElementById("get").addEventListener("focus", getFocus);
  document.getElementById("post").addEventListener("focus", postFocus);

  document.getElementById("get").addEventListener("click", getClick);
  document.getElementById("post").addEventListener("click", postClick);

  document.getElementById("fname").focus();
});

function inputFocus() {
  document.activeElement.select();
  displayPrompt();
  checkButtons();
}

function displayPrompt(id = null) {
  const prompts = {
    fname: "Enter your first name",
    lname: "Enter your last name",
    address: "Enter your address",
    city: "Enter your city",
    region: "Enter your region",
    postalCode: "Enter your postal code as xxxxx",
    phoneNumber: "Enter your phone number as xxx-xx-xxxx",
    dateOfBirth: "Enter your date of birth as xx-xx-xxxx"
  }

  let elements = document.getElementsByTagName("output");

  if (id == null) {
    id = document.activeElement.id;

    for (let i = 0; i < elements.length; i++) {
      elements[i].innerText = "";
    }
  }

  try {
    document.getElementById(id + "-prompt").innerText = prompts[id];
  }
  catch {
    // ignore when the active element is a button
  }
}

function inputInput() {
  let element = document.activeElement;
  let id = element.id;

  if (!element.checkValidity()) {
    document.getElementById(id + "-prompt").innerText = element.validationMessage;
  }
  else {
    document.getElementById(id + "-prompt").innerText = "";
  }
  checkButtons();
}

function checkButtons() {
  let elements = document.getElementsByTagName("input");

  for (let i = 0; i < elements.length; i++) {
    if (!elements[i].checkValidity()) {
      document.getElementById("get").disabled = true;
      document.getElementById("post").disabled = true;
      return;
    }
  }

  document.getElementById("get").disabled = false;
  document.getElementById("post").disabled = false;
}

function createUser() {
  let fname = document.getElementById('fname').value;
  let lname = document.getElementById('lname').value;
  let address = document.getElementById('address').value;
  let city = document.getElementById('city').value;
  let region = document.getElementById('region').value;
  let postalCode = document.getElementById('postalCode').value;
  let phoneNumber = document.getElementById('phoneNumber').value;
  let dateOfBirth = document.getElementById('dateOfBirth').value;

  let user = new User(
    fname,
    lname, 
    address,
    city, 
    region,
    postalCode,
    phoneNumber,
    dateOfBirth
    );

  return user;
}

function getFocus() {
  document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users/1";
  
  document.getElementById("data").innerText = "";
  document.getElementById("response").innerText = "";
}

function getClick() {
  let url = document.getElementById("url").innerText;
  let request = new XMLHttpRequest();
  request.open("GET", url);
  request.onload = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    document.getElementById("response").innerText += "responseText:\n" + request.responseText;
  };
  request.send(null);
}

function postFocus() {
  document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users";

  let user = createUser();
  document.getElementById("data").innerText = JSON.stringify(user, null, 2);
  document.getElementById("response").innerText = "";
}

function postClick() {
  let url = document.getElementById("url").innerText;
  let data = document.getElementById("data").innerText;
  let request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json");
  request.onreadystatechange = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    document.getElementById("response").innerText += "responseText:\n" + request.responseText;
  };
  request.send(data);
}