# Karyna Kazhamiakina

kkazhamiakina.bitbucket.io

## Assignment 1

I'm taking this course as it's a requirement in my web designer program. Planning to use Java Script at work or future classes if necessary. 

## Assignment 2

Completed Assignment 2. The class is really exciting right now and I cannot wait to use the knowledge and practice of the next assignments.
I have learned a lot about variables and liked the assignments a lot, even thou I had issues with the 2 arithmetics assignments, 
it took me some time to figure it out by googling similar tasks and getting examples. I also like that there's a lot of information provided in Wikiversity with helpful links to the videos. 
Even when it seems so complicated, I'm excited about the outcome and results when the code starts to finally work. :)

## Assignment 3

Completed Assignment 3. I liked learning about functions, as it's breaking down the code into the separate blocks, that perform different actions.
I have struggled with the 3rd activity, trying to create a program that calculates the distance based on the miles you input, but I've got it all figured out.
The multimedia examples were really helpful in Wikiversity for this chapter, as well as the examples from the W3Schools. 

## Assignment 4

Completed Assignment 4. That was an easy lesson for me to learn Events in JavaScript. I like the outcome that you can do with the events and that 
you don't need to add any other actions to make the code work. The YouTube examples are great on Wikiversity, especially those with activities examples. 
I'm getting more and more excited about this class and how many new things we can do with the code.

## Assignment 5

Completed Assignment 5. I liked learning about conditions, as it allows you to perform different actions based on different decisions. 
It was a new learning experience, which I really liked. Also, that's probably the first assignement where the code started to differ from what we did before.
The YouTube examples are very helpful on Wikiversity and W3Schools. I was also able to find good examples of the activities' code. I'm glad to see the outcome when the code starts to work.

## Assignment 6
Completed Assignment 6. It was interesting to learn about while loops and see how it helps to avoid code repetition. 
I didn't have any issues with this assignment, as the materials provided were very helpful, as well as the YouTube tutorials I found. Thank you!

## Assignment 7
Completed Assignment 7. That was an easy lesson for me to learn for loop in JavaScript. I like the outcome that you can do the tables and multiplications. 
It was a new learning experience, which I really liked. The YouTube examples are very helpful on Wikiversity and W3Schools. I was also able to find good examples of the activities' code.

## Assignment 8

Completed Assignment 8. There was a lot of new information about Arrays and it's getting more complicated for me now. 
The Wiki links are very helpful, however I had to search for some extra resourses on the internet to make sure I understand the topic. 
I will add some links that I find usefull to my Wiki edits.

## Assignment 9

Completed Assignment 9. I loved the outcome with those dates! It's getting more and more exciting with each lesson. There are a lot of great examples on the Internet, 
I'll attach some that I've found to Wiki. I really enjoyed learning this section. 

## Assignment 10

Completed Assignment 10. I liked learning about Objects and I like that it's getting more challenging. 
At the same time it's so exciting to learn how many things we can do in Java Script.
The Wiki links are really helpful and I will share some tutorils that I find usefull to my Wiki edits.

## Assignment 11

Completed Assignment 11 about DOM and BOM. It's been easier to complete the activities. The Wiki links are very helpful, however, 
I had to search for some extra resources on the internet to make sure I understand the topic. 
I would definitely use it in my future work by adding some animation to make the websites look cooler, 
as well as creating applications that update the data of the page without needing a refresh. 
I will add some links that I find useful to my Wiki edits. The knowledges will help me to organize and store data.
I can change the height and width properties to return the browser's window size. Transforms XML files into a tree structure, a hierarchy of nodes. 

## Assignment 12

Completed Assignment 12 about Dynamic HTML. It's been more complicated to complete the assignment. The Wiki links are very helpful. 
But it is very exciting to learn how many things we can do in Java Script. I will share some tutorials that I find useful to my Wiki edits.

## Assignment 13

Completed Assignment 13 about Forms and Security. It's been easier to complete the assignment. The Wiki links are very helpful. 
We've learned how to complete and debug code that retrieves input from forms and sets form field values. I will share some tutorials that I find useful to my Wiki edits.

## Assignment 14
Completed Assignment 14 about AJAX and JSON. It's hard to believe we are almost at the end of the semester. I have completed the first and second assignments. 
I will use the knowledge from this assignment to make the pages dynamic and refresh data in those web pages without refreshing the page; 
which will makes user pages interactive and create a slicker user experience for the user.

## Assignment 16 - Final Project

The project is a website for “Familu Pizza” where you can order pizza for delivery.

The following technologies were used for execution: HTML, CSS, JavaScript, AJAX, and JSON.

The site offers 6 types of pizzas, for each pizza you can choose any extra toppings to add (onions, tomatoes, chicken, mushrooms, bacon or cheese). 
There are also three pizza sizes available - medium (standard), large and extra large. 
You can order as many pizzas as you like, the order can be changed using the “Add to cart” and “Remove buttons”. 

Add the number of pizzas to the quantity field. The order amount is dynamically generated and immediately shown to the user. 
The total of the order itself is displayed separately in the “Order field” (it includes the cost of the pizza, toppings, pizza size and quantity), 
a 10% tax is calculated separately and displayed in the “Sales tax” (+ 10%) field, and the total order amount is displayed in “Total”. 

It is necessary to fill out a form with the fields name, address and phone - that are required, in order for the user to place an order. 
Validation of the fields has been done, if the user skips the fields or enters any incorrect data, he will receive a notification. 
There is also a field for entering comments. The order is made only when all the required fields are filled in and the pizzas are selected.

When you click the order button - “Order Pizza”, if the validation was successful, then an object is formed with all the data that the user has selected and entered. 
Then this one is translated into JSON format and transmitted using AJAX technology - XMLHttpRequest (). To check what was sent to the server, 
please refer to the bottom of the page - this data is displayed below under the “Your POST Data” field.

Below are the commits that were submitted with the comments to each.

1 - The first milestone I created was the basic page with pizza information.
2 - I've added the prices and ability to add to the card.
3 - I've used a form-layout to create a contact form. 
4 - I added a function to remove products from the cart. 
5 - I added a function to update the cart. 
6 - Price number udated.
7 - Total price update. 
8 - Checked the page loading.
9 - Separated remove function.
10 - Bug fix ready function.
11 - Dynamic update of total price.
12 - Prohibition of negative.
13 - Add to cart button.
14 - Add item to cart.
15 - Cart styling.
16 - Duplicate order verification.
17 - Updating of just added pizzas.
18 - Updating the order button.
19 - Adding toppings and size styles.
20 - Adding toppings and size to total.
21 - Bug fix updating of all toppings and sizes for all selected pizzas.
22 - Created object with all the info.
23 - Sales tax.
24 - Form validation.
25 - Ajax and Json.

I am planning to use the knowledge gained in this course at any work I will be doing as a web designer. 
Java Script turned out to be such a powerful language that allows you to do so many things around the website.
It can help to run multiple tasks per one. It also extends the functionalities of the websites. 
fff
